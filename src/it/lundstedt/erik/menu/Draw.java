package it.lundstedt.erik.menu;

import java.util.function.Consumer;

public class Draw
{

	public static final String ANSI_RESET = "\u001B[0m";
	public static final String ANSI_BLACK = "\u001B[30m";
	public static final String ANSI_RED = "\u001B[31m";
	public static final String ANSI_GREEN = "\u001B[32m";
	public static final String ANSI_YELLOW = "\u001B[33m";
	public static final String ANSI_BLUE = "\u001B[34m";
	public static final String ANSI_PURPLE = "\u001B[35m";
	public static final String ANSI_CYAN = "\u001B[36m";
	public static final String ANSI_WHITE = "\u001B[37m";

	public static final String ANSI_BLACK_BACKGROUND = "\u001B[40m";
	public static final String ANSI_RED_BACKGROUND = "\u001B[41m";
	public static final String ANSI_GREEN_BACKGROUND = "\u001B[42m";
	public static final String ANSI_YELLOW_BACKGROUND = "\u001B[43m";
	public static final String ANSI_BLUE_BACKGROUND = "\u001B[44m";
	public static final String ANSI_PURPLE_BACKGROUND = "\u001B[45m";
	public static final String ANSI_CYAN_BACKGROUND = "\u001B[46m";
	public static final String ANSI_WHITE_BACKGROUND = "\u001B[47m";
	static int totLength=40;
	void setTotLength(int goal)
	{
		this.totLength=goal;
	}

	int getTotLength()
	{
	return this.totLength;
	}


	static String padding="**************************************";
	public static Consumer<String> printer = text ->{
		System.out.println(text);
	};

	public static void setPrinter(Consumer<String> printer) {
		Draw.printer = printer;
	}

	public void setPadding()
	{
		this.padding="";
		for (int i = 0; i <totLength-2 ; i++)
		{
			this.padding=this.padding+"*";
		}
	}
	public static String getPadding()
	{return padding;}





	public static void headder(String[] input)
	{
		String frame="*";
		printer.accept(contentloop(frame,totLength));
		for (int i = 0; i <input.length ; i++) {
			content(input[i]);
		}
		printer.accept(contentloop(frame,totLength));
	}

	public static void content(String input)
	{
		String blank=" ";
		int len=input.length();
		switch (len%2){
			case 0:
				break;
			case 1:
				input=input.concat(blank);
				break;
		}
		len=input.length();
		int padAmount=totLength/2-len/2;
		printer.accept("*"+contentloop(blank, padAmount-1)+input+contentloop(blank, padAmount-1)+"*");
	}

	private static String contentloop(String blank, int padAmount) {
		String result="";
		for (int i = 0; i <padAmount ; i++)
		{
			result +=blank;
		}
		return result;
	}
}
