package it.lundstedt.erik.menu;
import java.util.Scanner;
import java.util.function.Consumer;


public class Menu
{
	/*
	* keep
	* for
	* licence
	*/

	public static Draw drawingObj=new Draw();
	/*example formatting*/
	public static final String VERSION ="V1.0.0";
	String[] header = {"WELCOME TO OpenMenu", "version "+ VERSION, "its free and", "open source", "under the don't be a jerk licence"};
	String[] menuItems={"first","second","third"};
	/*end example formating*/


public Menu() {
}



public Menu(String[] header, String[] menuItems) {
	this.header = header;
	this.menuItems = menuItems;
	this.setHeader(header);
	this.setMenuItems(menuItems);
}


public void setPrinter(Consumer<String> printer) {
		drawingObj.printer = printer;
	}

	/*end example formating*/
	public String[] getHeader()
	{
		return header;
	}
	public void setHeader(String[] header)
	{
		this.header = header;
	}
	public String[] getMenuItems() {
		return menuItems;
	}
	public void setMenuItems(String[] menuItems)
	{
		this.menuItems = menuItems;
	}
	
	
	public static void licence()//use this at the start of a program with the don't be a jerk licence to get a nice looking licence headder
	{
		drawingObj.setTotLength(36);
		drawingObj.setPadding();
		String[] licenceHeader={"WELCOME TO OpenMenu","version 6.0","its free and","open source","under the don't be a jerk licence"};
		drawHeader(licenceHeader);
		
	}
	public static void licence(String progName,String verno)//use this at the start of a program with the don't be a jerk licence to get a nice looking licence headder
	{
		drawingObj.setTotLength(36);
		drawingObj.setPadding();
		String[] licenceHeader={"","version "+verno,"its free and","open source","under the don't be a jerk licence"};
		licenceHeader[0]=progName;
		drawHeader(licenceHeader);
	}

	public void setTotLength(int goal)
	{
		drawingObj.setTotLength(goal);
		drawingObj.setPadding();
	}

public int getTotLength()
{
	return drawingObj.getTotLength();
}

	public void drawMenu()
	{
		drawingObj.headder(header);
		for (String menuItem : menuItems)
		{
			drawingObj.content(menuItem);
		}
		drawingObj.content(drawingObj.getPadding());
	}

	public static void drawMenu(String[]header,String[]menuItems)
	{
		drawingObj.headder(header);
		for (String menuItem : menuItems) {
			drawingObj.content(menuItem);
		}
		drawingObj.content(drawingObj.getPadding());
	}
	public static void drawItems(String[]menuItems)
	{
		for (String menuItem : menuItems) {
			drawingObj.content(menuItem);
		}
	}
	public void drawItems()
	{
		for (String menuItem : this.menuItems) {
			drawingObj.content(menuItem);
		}
	}

	public static void drawItem(String menuItem)
	{
			drawingObj.content(menuItem);
	}
	public static void drawHeader(String[]header)
	{
		drawingObj.headder(header);
	}
	
	
	public static void debug(String content,boolean debug)
	{
		if (debug) {
			System.out.print("\u001B[31m");
			drawingObj.content(drawingObj.padding);
			drawingObj.content(content);
			drawingObj.content(drawingObj.padding);
			System.out.print("\u001B[0m");
		}
	}
	
	public static void error(String content)
	{
			System.out.print("\u001B[31m");
			drawingObj.content(drawingObj.padding);
			drawingObj.content(content);
			drawingObj.content(drawingObj.padding);
			System.out.print("\u001B[0m");
	}
	
	public static void important(String content)
	{
		System.out.print(drawingObj.ANSI_RED_BACKGROUND);
		drawingObj.content(drawingObj.padding);
		drawingObj.content(content);
		drawingObj.content(drawingObj.padding);
		System.out.print(drawingObj.ANSI_RESET);
	}
	public static int getChoice()
	{
		Scanner input = new Scanner(System.in);
		int MenyVal = input.nextInt();
		return MenyVal;
	}

	public static String getChoiceStr()
	{
		Scanner input = new Scanner(System.in);
		String MenyVal = input.nextLine();
		return MenyVal;
	}
	// Display a message and get the users input.
	public static String getInput(String message) {
		Menu.drawItem(message);
		Scanner in = new Scanner(System.in);
		String input = in.next();
		return input;
	}


}
