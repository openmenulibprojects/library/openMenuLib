package it.lundstedt.erik.menu;

public class Utils
{
public static boolean quit()
{
	String[] header={"do you want to exit?"};
	String[] menuItems={"yes/y","no/n"};

	Menu quitMenu=new Menu();
	quitMenu.setHeader(header);
	quitMenu.setMenuItems(menuItems);
	quitMenu.drawMenu();
	String doQuit=quitMenu.getChoiceStr();
	switch (doQuit)
	{
		case "y":
		case "yes":
		case "Y":
		case "Yes":
			return true;
		case "n":
		case "no":
		case "N":
		case "No":
			return false;
		default:
			throw new IllegalStateException("Unexpected value: " + doQuit);
	}
}



	public static void exit(String[] args,boolean doDebug)
	{
		System.exit(0);
	}




}
