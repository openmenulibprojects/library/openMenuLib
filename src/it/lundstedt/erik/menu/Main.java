package it.lundstedt.erik.menu;

public class Main
{
	public static final String VERSION =Menu.VERSION;



	public static void testDebug(String[] header,String[] menuItems)
	{
		Menu mainMenu=new Menu();
		mainMenu.setHeader(header);
		mainMenu.setMenuItems(menuItems);
		mainMenu.drawMenu();
		mainMenu.setTotLength(40);
		Menu.debug("this is a debug", DO_DEBUG);
		Menu.error("this is an error");
		System.out.print(Draw.ANSI_BLUE);
		Draw.content(Draw.padding);
		Menu.drawItem("hello world");
		Draw.content(Draw.padding);
		System.out.print(Draw.ANSI_RESET);
	}
	public static void testANSI(String[] header,String[] menuItems)
	{

		Menu mainMenu=new Menu();

		System.out.print(Draw.ANSI_BLUE);
		Draw.content(Draw.padding);
		mainMenu.drawItem("hello world");
		Draw.content(Draw.padding);
		System.out.print(Draw.ANSI_RESET);

	}

	public static void testCustomLength(String[] header,String[] menuItems)
	{
		Menu mainMenu=new Menu();
		mainMenu.setHeader(header);
		mainMenu.setMenuItems(menuItems);
		mainMenu.setTotLength(34);
		mainMenu.drawMenu();
//Menu.drawItem();
		System.out.println(33/2+" : "+33%2);
		System.out.println(34/2+" : "+34%2);
		System.out.println(36/2+" : "+34%2);
		mainMenu.setTotLength(36);
		mainMenu.drawMenu();
		mainMenu.debug("this is a debug",true);
		mainMenu.error("this is an error");
	}

	public static void testLowLevel(String[] header,String[] menuItems)
	{
		Menu mainMenu=new Menu();
		mainMenu.setTotLength(40);
		mainMenu.setHeader(header);
		mainMenu.setMenuItems(menuItems);
		mainMenu.drawMenu();
		int i=0;
		menuItems[i]="> "+menuItems[i]+" <";
		mainMenu.setMenuItems(menuItems);
		mainMenu.drawItems();
	}

		public static final boolean DO_DEBUG = true;
	public static void  main(String[] args) {
		/*example formatting*/

		Menu.licence("openMenu", VERSION);


		String[] header = {"WELCOME TO OpenMenu", "version "+ VERSION, "its free and", "open source", "under the don't be a jerk licence"};
		String[] menuItems = {"first", "second", "third"};
		Menu.drawItem("**debug:**");
		testDebug(header,menuItems);

		Menu.drawItem("**ANSI:**");
		testANSI(header,menuItems);
		Menu.drawItem("**length:**");
		testCustomLength(header,menuItems);
		Menu.drawItem("**low-level:**");
		testLowLevel(header,menuItems);



		if (args.length > 0) {
			if (args[0].equals("keep")) {
				Menu.debug("kept", DO_DEBUG);
				boolean doQuit = Utils.quit();
				if (doQuit) {
					System.exit(0);
				} else if (!doQuit) {
					String[] str = {"keep"};
					Main.main(str);
				}
			} else {
				System.exit(0);
			}

		}
	}

	
	
	
}
